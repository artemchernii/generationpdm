$(function () {
	$('.artists_carousel').slick({
		dots: false,
		arrows: false,
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		nextArrow: '.artists_carousel__next',
		prevArrow: '.artists_carousel__prev',
		responsive: [{
				breakpoint: 5280,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: false,
					arrows: true
				}
			}, {
				breakpoint: 2280,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: false,
					arrows: true
				}
			}, {
				breakpoint: 2000,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: false,
					arrows: true
				}
			}, {
				breakpoint: 1680,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: false,
					arrows: true
				}
			}, {
				breakpoint: 1480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: false,
					arrows: true
				}
			},
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: false,
					arrows: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			}
		]
	});
	// $('.artists_carousel').owlCarousel({
	// 	loop: true,
	// 	margin: 10,
	// 	autoplay: false,
	// 	nav: false,
	// 	dots: false,
	// 	// navText: ['<i class="fas fa-angle-right next"></i>', '<i class="fas fa-angle-left prev"></i>'],
	// 	responsive: {
	// 		0: {
	// 			items: 2
	// 		},
	// 		600: {
	// 			items: 2
	// 		},
	// 		1000: {
	// 			items: 2
	// 		}
	// 	}
	// })
});


let body = document.getElementsByTagName("BODY")[0],
	// header = document.querySelector(".header"),
	main = document.querySelector(".main"),
	footer = document.querySelector(".footer"),
	spTitle = document.querySelector(".startpage_title"),
	spVideo = document.querySelector(".startpage_video");

window.onload = function () {
	spVideo.src = "img/olho_sorteio.gif" + "?a=" + Math.random();
	setTimeout(function () {
		window.scrollTo(0, 0);
	}, 100);
	setTimeout(function () {
		spTitle.classList.add("opacity0");
	}, 1500)
	setTimeout(function () {
		spTitle.style.display = "none";
	}, 2400)
	setTimeout(function () {
		spVideo.classList.add("opacity");
		spVideo.style.display = "block";
	}, 2400);
	setTimeout(function () {
		body.style.overflow = "auto";
		main.style.opacity = "1";
		footer.style.opacity = "1";
		window.scrollTo(0, 0);
	}, 4700);
	setTimeout(function () {
		spVideo.classList.add("opacity0");
	}, 4200)
	setTimeout(function () {
		spVideo.style.display = "none";
	}, 4800);
	// setTimeout(function () {
	// 	header.classList.add("opacity");
	// }, 6300)
	// var playButton = document.querySelector(".video_playbtn"),
	// 	video = document.querySelector(".show_video");
	// // Event listener for the play/pause button
	// playButton.addEventListener("click", function () {
	// 	video.play();
	// 	playButton.style.display = "none";
	// });
	setTimeout(function () {
		document.querySelector(".about_title").style.display = "none";
	}, 4200)
	setTimeout(function () {
		document.querySelector(".about_content").style.display = "block";
		document.querySelector(".about_title").style.display = "block";

	}, 4400)
}